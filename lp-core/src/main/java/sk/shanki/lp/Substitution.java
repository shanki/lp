/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Mutable.
 *
 * @author shanki
 */
public class Substitution {

    private final Map<Variable, Term> map;

    public Substitution() {
        map = new HashMap<>();
    }

    public Substitution(Substitution other) {
        Objects.requireNonNull(other, "other cannot be null");

        map = new HashMap<>(other.map);
    }

    public boolean put(Variable variable, Term value) {
        Objects.requireNonNull(variable, "variable cannot be null");
        Objects.requireNonNull(value, "value cannot be null");

        if (map.containsKey(variable)) {
            Term otherValue = map.get(variable);

            return otherValue.unify(value, this);
        } else {
            Term subEl = value.partiallySubstitute(this);
            map.put(variable, subEl);

            for (Variable var : map.keySet()) {
                Term olde = map.get(var);
                Term newe = olde.partiallySubstitute(this);
                map.put(var, newe);
            }

            return true;
        }
    }

    public Term get(Variable variable) {
        Objects.requireNonNull(variable, "variable cannot be null");

        return map.get(variable);
    }

    public Iterable<Variable> variables() {
        return map.keySet();
    }

    @Override
    public String toString() {
        return map.toString();
    }

    boolean contains(Variable variable) {
        Objects.requireNonNull(variable, "variable cannot be null");

        return map.containsKey(variable);
    }

}

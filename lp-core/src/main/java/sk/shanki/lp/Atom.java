/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Immutable.
 *
 * @author shanki
 */
public class Atom extends BaseLiteral {

    private final String symbol;
    private final Terms terms;

    private transient String type;
    private transient int hashCode;
    private transient Neg negated;
    private transient Not defaultNegated;

    public Atom(String symbol, Term... terms) {
        Objects.requireNonNull(symbol, "symbol cannot be null");
        Objects.requireNonNull(terms, "terms cannot be null");

        this.symbol = symbol;
        this.terms = Terms.fromWithOwnership(terms);
    }

    public Atom(String symbol, Terms terms) {
        Objects.requireNonNull(symbol, "symbol cannot be null");
        Objects.requireNonNull(terms, "terms cannot be null");

        this.symbol = symbol;
        this.terms = terms;
    }

    @Override
    public boolean isPositive() {
        return true;
    }

    @Override
    public boolean isNegated() {
        return false;
    }

    @Override
    public boolean isOfSymbol(String predicateName) {
        return symbol.equals(predicateName);
    }

    @Override
    public String getSymbol() {
        return symbol;
    }

    @Override
    public int getArity() {
        return terms.size();
    }

    @Override
    public Term getTerm(int index) {
        return terms.get(index);
    }

    @Override
    public <T extends Term> T getTerm(int index, Class<T> clazz) {
        return (T) getTerm(index);
    }

    @Override
    public int getIntTerm(int index) {
        return getTerm(index, NumberConstant.class).intValue();
    }

    @Override
    public BigDecimal getNumberTerm(int index) {
        return getTerm(index, NumberConstant.class).getValue();
    }

    @Override
    public String getStringTerm(int index) {
        return getTerm(index, StringConstant.class).getValue();
    }

    @Override
    public String getSymbolicTerm(int index) {
        return getTerm(index, FunctionalTerm.class).getSymbol();
    }

    @Override
    public boolean unify(Literal literal, Substitution substitution) {

        if (literal instanceof Atom == false) {
            return false;
        }

        Atom other = (Atom) literal;

        if (this.symbol.equals(other.symbol) == false) {
            return false;
        }

        return terms.unify(other.terms, substitution);
    }

    @Override
    public Atom fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Terms substitutedTerms = terms.fullySubstitute(substitution);

        // object allocation optimization
        return terms == substitutedTerms ? this : new Atom(symbol, substitutedTerms);

    }

    @Override
    public Atom partiallySubstitute(Substitution substitution) {
        Terms substitutedTerms = terms.partiallySubstitute(substitution);

        // object allocation optimization
        return terms == substitutedTerms ? this : new Atom(symbol, substitutedTerms);
    }

    @Override
    public boolean isGround() {
        return terms.isGround();
    }

    @Override
    public Atom rewriteIdsToObjectConstants(ObjectConstantMapping mapping) {
        Terms rewrittenTerms = terms.rewriteIdsToObjectConstants(mapping);

        // object allocation optimization
        return terms == rewrittenTerms ? this : new Atom(symbol, rewrittenTerms);
    }

    public Literal unwrap() {
        return (Literal) terms.get(0);
    }

    @Override
    public String getType() {
        if (type == null) {
            type = symbol + "/" + terms.size();
        }

        return type;
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 7;
            hash = 19 * hash + symbol.hashCode();
            hash = 19 * hash + terms.hashCode();

            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Atom other = (Atom) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        if (!this.symbol.equals(other.symbol)) {
            return false;
        }

        return this.terms.equals(other.terms);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printTuple(symbol, terms);
    }

    @Override
    public Atom rewriteObjectConstantsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        Terms rewrittenTerms = terms.rewriteObjectConstantsToIds(mapping);

        // object allocation optimization
        return terms == rewrittenTerms ? this : new Atom(symbol, rewrittenTerms);
    }

    @Override
    public boolean willChange(Substitution substitution) {
        return terms.willChange(substitution);
    }

    @Override
    public Atom getAtom() {
        return this;
    }

    public Rule toMetaComplement() {
        return new Rule(new Atom("compl", new ObjectConstant(this), new ObjectConstant(this.negate())));
    }

    @Override
    public Neg negate() {
        if (negated == null) {
            negated = new Neg(this);
        }

        return negated;
    }

    @Override
    public Not defaultNegate() {
        if (defaultNegated == null) {
            return new Not(this);
        }

        return defaultNegated;
    }

}

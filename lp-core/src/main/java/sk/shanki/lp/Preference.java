/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class Preference {

    private final Rule less;
    private final Rule more;

    private transient int hashCode;
    private transient String string;

    public Preference(Rule less, Rule more) {
        this.less = Objects.requireNonNull(less, "less cannot be null");
        this.more = Objects.requireNonNull(more, "more cannot be null");
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 3;
            hash = 97 * hash + less.hashCode();
            hash = 97 * hash + more.hashCode();
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Preference other = (Preference) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        if (!Objects.equals(this.less, other.less)) {
            return false;
        }
        return Objects.equals(this.more, other.more);
    }

    public Rule getLess() {
        return less;
    }

    public Rule getMore() {
        return more;
    }

    Rule toMetaRule() {
        return new Rule(new Atom("pr", new ObjectConstant(more), new ObjectConstant(less)));
    }

    @Override
    public String toString() {
        if (string == null) {
            string = less.getName() + " < " + more.getName();
        }

        return string;
    }

    public void print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        printer.printPrefence(less, more);
    }

}

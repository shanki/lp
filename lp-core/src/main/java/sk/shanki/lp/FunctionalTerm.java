/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Arrays;
import java.util.Objects;
import sk.shanki.lp.exceptions.UnboundVariableException;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class FunctionalTerm extends BaseTerm {

    private final String symbol;
    private final Terms terms;

    private transient int hashCode;

    public FunctionalTerm(String symbol, Term... terms) {
        Objects.requireNonNull(symbol, "symbol cannot be null");
        Objects.requireNonNull(terms, "terms cannot be null");

        this.symbol = symbol;
        this.terms = Terms.fromWithOwnership(terms);
    }

    public FunctionalTerm(String symbol, Terms terms) {
        Objects.requireNonNull(symbol, "symbol cannot be null");
        Objects.requireNonNull(terms, "terms cannot be null");

        this.symbol = symbol;
        this.terms = terms;
    }

    @Override
    public FunctionalTerm fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substition cannot be null");

        // object allocation optimization
        Terms substitutedTerms = terms.fullySubstitute(substitution);
        return terms == substitutedTerms ? this : new FunctionalTerm(symbol, substitutedTerms);
    }

    @Override
    public FunctionalTerm partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substition cannot be null");

        // object allocation optimization
        Terms substitutedTerms = terms.partiallySubstitute(substitution);
        return terms == substitutedTerms ? this : new FunctionalTerm(symbol, substitutedTerms);
    }

    @Override
    public boolean isGround() {
        return terms.isGround();
    }

    public boolean hasPredicate(String predicate) {
        Objects.requireNonNull(predicate, "precidate cannot be null");

        return this.symbol.equals(predicate);
    }

    public Term getTerm(int index) {
        return terms.get(index);
    }

    @Override
    public Term rewriteIdsToObjectConstants(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        ObjectConstant constant = mapping.fromId(this);

        if (constant != null) {
            return constant;
        }

        // object allocation optimization
        Terms rewrittenTerms = terms.rewriteIdsToObjectConstants(mapping);
        return terms == rewrittenTerms ? this : new FunctionalTerm(symbol, rewrittenTerms);
    }

    public Literal unwrap() {
        return (Literal) terms.get(0);
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 7;
            hash = 19 * hash + symbol.hashCode();
            hash = 19 * hash + terms.hashCode();
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final FunctionalTerm other = (FunctionalTerm) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        if (!this.symbol.equals(other.symbol)) {
            return false;
        }
        return this.terms.equals(other.terms);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printTuple(symbol, terms);
    }

    @Override
    public FunctionalTerm rewriteObjectConstantsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        return new FunctionalTerm(symbol, terms.rewriteObjectConstantsToIds(mapping));
    }

    @Override
    public boolean willChange(Substitution substitution) {
        Objects.requireNonNull(substitution, "substition cannot be null");

        return terms.willChange(substitution);
    }

    @Override
    public int getPriority() {
        return 12;
    }

    @Override
    public int compareTo(Term element, ObjectConstantMapping mapping) {
        Objects.requireNonNull(element, "element cannot be null");
        Objects.requireNonNull(mapping, "mapping cannot be null");

        if (element instanceof FunctionalTerm) {
            FunctionalTerm other = (FunctionalTerm) element;

            int a = this.symbol.compareTo(other.symbol);
            if (a != 0) {
                return a;
            }

            return this.terms.compareTo(other.terms, mapping);
        } else {
            return this.compareToDifferent(element);
        }
    }

    public int getArity() {
        return terms.size();
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public boolean unify(Term term, Substitution substitution) {
        Objects.requireNonNull(term, "term cannot be null");
        Objects.requireNonNull(substitution, "substitution cannot be null");

        if (term instanceof FunctionalTerm == false) {
            return false;
        }

        FunctionalTerm other = (FunctionalTerm) term;

        if (this.symbol.equals(other.symbol) == false) {
            return false;
        }

        return terms.unify(other.terms, substitution);
    }

}

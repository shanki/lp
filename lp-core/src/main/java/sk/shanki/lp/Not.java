/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;
import sk.shanki.lp.printing.StandardProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class Not implements NafLiteral {

    private final Literal literal;
    private transient int hashCode;

    public Not(Literal literal) {
        this.literal = Objects.requireNonNull(literal, "literal cannot be null");
    }

    @Override
    public Literal getLiteral() {
        return literal;
    }

    @Override
    public Atom getAtom() {
        return literal.getAtom();
    }

    @Override
    public boolean isGround() {
        return literal.isGround();
    }

    @Override
    public boolean isSatisfiedIn(AnswerSet answerSet) {
        return answerSet.contains(literal) == false;
    }

    @Override
    public NafLiteral fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        Literal substitutedLiteral = literal.fullySubstitute(substitution);
        return literal == substitutedLiteral ? this : substitutedLiteral.defaultNegate();
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 7;
            hash = 37 * hash + literal.hashCode();
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Not other = (Not) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        return Objects.equals(this.literal, other.literal);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printNot(literal);
    }

    @Override
    public Not rewriteObjectConstantsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        Literal rewrittenLiteral = literal.rewriteObjectConstantsToIds(mapping);
        return literal == rewrittenLiteral ? this : rewrittenLiteral.defaultNegate();
    }

    @Override
    public Not partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        Literal substitutedLiteral = literal.partiallySubstitute(substitution);
        return literal == substitutedLiteral ? this : substitutedLiteral.defaultNegate();
    }

    @Override
    public boolean willChange(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        return literal.willChange(substitution);
    }

    public Not rewriteIdsToObjects(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        Literal rewrittenLiteral = literal.rewriteIdsToObjectConstants(mapping);
        return literal == rewrittenLiteral ? this : rewrittenLiteral.defaultNegate();
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import sk.shanki.lp.exceptions.UnboundVariableException;

/**
 *
 * @author shanki
 */
public interface Literal extends NafLiteral {

    // Access methods
    //
    public boolean isPositive();

    public boolean isNegated();

    public boolean isOfSymbol(String symbol);

    public String getSymbol();

    public int getArity();

    public Term getTerm(int index);

    public <T extends Term> T getTerm(int index, Class<T> clazz);

    public String getSymbolicTerm(int index);

    public String getStringTerm(int index);

    public int getIntTerm(int index);

    public BigDecimal getNumberTerm(int index);

    // Substitution methods
    //
    boolean unify(Literal other, Substitution substitution);

    @Override
    public Literal fullySubstitute(Substitution substitution) throws UnboundVariableException;

    @Override
    public Literal partiallySubstitute(Substitution substitution);

    // Other methods
    //
    @Override
    public Literal rewriteObjectConstantsToIds(ObjectConstantMapping mapping);

    public Literal rewriteIdsToObjectConstants(ObjectConstantMapping mapping);

    public Atom wrap(String name);

    public Literal negate();

    public Not defaultNegate();

    public Rule toFactRule();

    public String getType();

    public boolean isOfType(String type);

}

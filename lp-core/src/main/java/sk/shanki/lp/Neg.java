/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class Neg extends BaseLiteral {

    private final Atom atom;
    private transient String type;
    private transient int hashCode;
    private transient Not defaultNegated;

    public Neg(Atom atom) {
        this.atom = Objects.requireNonNull(atom, "atom cannot be null");
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    public boolean isNegated() {
        return true;
    }

    @Override
    public boolean isOfSymbol(String predicateName) {
        Objects.requireNonNull(predicateName, "predicateName cannot be null");

        return atom.isOfSymbol(predicateName);
    }

    @Override
    public String getSymbol() {
        return atom.getSymbol();
    }

    @Override
    public int getArity() {
        return atom.getArity();
    }

    @Override
    public Term getTerm(int index) {
        return atom.getTerm(index);
    }

    @Override
    public <T extends Term> T getTerm(int index, Class<T> clazz) {
        Objects.requireNonNull(clazz, "clazz cannot be null");

        return atom.getTerm(index, clazz);
    }

    @Override
    public int getIntTerm(int index) {
        return atom.getIntTerm(index);
    }

    @Override
    public BigDecimal getNumberTerm(int index) {
        return atom.getNumberTerm(index);
    }

    @Override
    public String getStringTerm(int index) {
        return atom.getStringTerm(index);
    }

    @Override
    public String getSymbolicTerm(int index) {
        return atom.getSymbolicTerm(index);
    }

    @Override
    public boolean unify(Literal literal, Substitution substitution) {
        Objects.requireNonNull(literal, "literal cannot be null");
        Objects.requireNonNull(substitution, "substitution cannot be null");

        if (literal instanceof Neg == false) {
            return false;
        }

        Neg other = (Neg) literal;

        return atom.unify(other.atom, substitution);
    }

    @Override
    public Neg fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        Atom substituted = atom.fullySubstitute(substitution);

        return atom == substituted ? this : substituted.negate();
    }

    @Override
    public Neg partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        Atom substituted = atom.partiallySubstitute(substitution);

        return atom == substituted ? this : substituted.negate();
    }

    @Override
    public String getType() {
        if (type == null) {
            type = "-" + atom.getType();
        }

        return type;
    }

    @Override
    public boolean isGround() {
        return atom.isGround();
    }

    @Override
    public Atom getAtom() {
        return atom;
    }

    @Override
    public Neg rewriteIdsToObjectConstants(ObjectConstantMapping cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        Atom rewrittenAtom = atom.rewriteIdsToObjectConstants(cache);

        return atom == rewrittenAtom ? this : rewrittenAtom.negate();
    }

    @Override
    public Atom negate() {
        return atom;
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 7;
            hash = 23 * hash + atom.hashCode();

            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Neg other = (Neg) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        return this.atom.equals(other.atom);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printNeg(atom);
    }

    @Override
    public Neg rewriteObjectConstantsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        Atom rewrittenAtom = atom.rewriteObjectConstantsToIds(mapping);

        return atom == rewrittenAtom ? this : rewrittenAtom.negate();
    }

    @Override
    public boolean willChange(Substitution substitution) {
        return atom.willChange(substitution);
    }

    @Override
    public Not defaultNegate() {
        if (defaultNegated == null) {
            defaultNegated = new Not(this);
        }

        return defaultNegated;
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;

/**
 *
 * Mutable.
 *
 * @author shanki
 */
public class Collector {

    private final WeakConstraints weaks;
    private final int maxAnswerSets;

    private final AnswerSets ass = new AnswerSets();
    private ViolationDegree bestDegree;

    public Collector(WeakConstraints weaks, int maxAnswerSets) {
        Objects.requireNonNull(weaks, "weaks cannot be null");

        if (maxAnswerSets < 0) {
            throw new IllegalArgumentException("maxAnswer sets must be at least 0");
        }

        this.weaks = weaks;
        this.maxAnswerSets = maxAnswerSets;
    }

    public void add(AnswerSet as) {
        Objects.requireNonNull(as, "as cannot be null");

        ViolationDegree degree = weaks.computeViolationDegreeOf(as);

        int c = degree.compareTo(bestDegree, -1);

        if (c < 0) {
            ass.clear();
            ass.add(as);
            bestDegree = degree;
        } else if (c == 0) {
            ass.add(as);
        }
    }

    public boolean searchNext() {
        if (weaks.isEmpty()) {
            return maxAnswerSets == 0 || ass.size() < maxAnswerSets;
        } else {
            return true;
        }
    }

    public AnswerSets getAnswerSets() {
        if (weaks.isEmpty() == false || maxAnswerSets == 0) {
            return ass;
        } else {
            int count = Math.min(ass.size(), maxAnswerSets);
            AnswerSets sel = new AnswerSets(count);

            for (AnswerSet a : ass) {
                if (sel.size() < maxAnswerSets) {
                    sel.add(a);
                } else {
                    break;
                }
            }
            return sel;
        }
    }
}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Immutable.
 *
 * @author shanki
 */
public class StringConstant extends BaseConstant {

    private static final StringConstant EMPTY = new StringConstant("");

    public static final StringConstant empty() {
        return EMPTY;
    }

    public static final StringConstant instanceFor(String string) {
        Objects.requireNonNull(string, "string cannot be null");

        if (string.isEmpty()) {
            return empty();
        } else {
            return new StringConstant(string);
        }
    }

    private final String string;
    private transient int hashCode;

    private StringConstant(String string) {
        assert (string != null);

        this.string = string;
    }

    public StringConstant(char c) {
        this.string = Character.toString(c);
    }

    public String getValue() {
        return string;
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = 679 + string.hashCode();
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StringConstant other = (StringConstant) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        return this.string.equals(other.string);
    }

    @Override
    public int compareTo(Term other, ObjectConstantMapping cache) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(cache, "cache cannot be null");

        if (other instanceof StringConstant) {
            StringConstant o = (StringConstant) other;
            return o.string.compareTo(o.string);
        } else {
            return compareToDifferent(other);
        }
    }

    public StringConstant append(StringConstant other) {
        Objects.requireNonNull(other, "other cannot be null");

        if (isEmpty()) {
            return other;
        }

        if (other.isEmpty()) {
            return this;
        }

        return StringConstant.instanceFor(string + other.string);
    }

    public boolean isEmpty() {
        return string.isEmpty();
    }

    public StringConstant remove() {
        if (isEmpty()) {
            return empty();
        }

        String ret = string.substring(0, string.length() - 1);

        return StringConstant.instanceFor(ret);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printStringConstant(string);
    }

    @Override
    public int getPriority() {
        return 2;
    }

}

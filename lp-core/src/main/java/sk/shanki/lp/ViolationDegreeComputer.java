/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author shanki
 */
class ViolationDegreeComputer {

    private final SortedMap<BigDecimal, Set<WeakConstraintTuple>> tuples = new TreeMap<>();

    void consume(WeakConstraint weak, boolean isViolated) {
        Objects.requireNonNull(weak, "weak cannot be null");

        BigDecimal level = weak.getLevel();

        Set<WeakConstraintTuple> set = tuples.get(level);
        if (set == null) {
            set = new HashSet<>();
            tuples.put(level, set);
        }

        if (isViolated) {
            set.add(weak.toTuple());
        }
    }

    ViolationDegree getValue() {
        List<BigDecimal> weights = new ArrayList<>(tuples.size());

        for (Set<WeakConstraintTuple> set : tuples.values()) {

            BigDecimal weight = BigDecimal.ZERO;

            for (WeakConstraintTuple tuple : set) {
                weight = weight.add(tuple.getWeight());
            }

            weights.add(weight);
        }

        return ViolationDegree.fromWithOwnership(weights);
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;
import sk.shanki.lp.printing.StandardProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public abstract class BaseLiteral implements Literal {

    @Override
    public Atom wrap(String name) {
        Objects.requireNonNull(name, "name cannot be null");

        return new Atom(name, new ObjectConstant(this));
    }

    @Override
    public Literal getLiteral() {
        return this;
    }

    @Override
    public boolean isOfType(String signature) {
        Objects.requireNonNull(signature, "signature cannot be null");

        return getType().equals(signature);
    }

    @Override
    public Rule toFactRule() {
        return new Rule(this);
    }

    @Override
    public boolean isSatisfiedIn(AnswerSet answerSet) {
        Objects.requireNonNull(answerSet, "answerSet cannot be null");

        return answerSet.contains(this);
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

}

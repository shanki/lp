/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author shanki
 */
class WeakConstraintTuple {

    private final BigDecimal weight;
    private final Terms terms;

    private transient int hashCode;

    WeakConstraintTuple(BigDecimal weight, Terms terms) {
        this.weight = Objects.requireNonNull(weight, "weight cannot be null");
        this.terms = Objects.requireNonNull(terms, "terms cannot be null");
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 3;
            hash = 97 * hash + weight.hashCode();
            hash = 97 * hash + terms.hashCode();
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WeakConstraintTuple other = (WeakConstraintTuple) obj;
        if (!this.weight.equals(other.weight)) {
            return false;
        }
        if (!this.terms.equals(other.terms)) {
            return false;
        }
        return true;
    }

    BigDecimal getWeight() {
        return weight;
    }
}

package sk.shanki.lp.util;

/**
 *
 * @author shanki
 */
public class Validation {

    public static <E> void requireAllNonNull(Iterable<? extends E> elements, String message) {
        for (E element : elements) {
            if (element == null) {
                throw new NullPointerException(message);
            }
        }
    }

    public static <E> void requireAllNonNull(E[] elements, String message) {
        for (E element : elements) {
            if (element == null) {
                throw new NullPointerException(message);
            }
        }
    }

    private Validation() {
        // prevent instance
    }
}

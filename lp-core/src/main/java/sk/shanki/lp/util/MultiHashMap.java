/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author shanki
 */
public class MultiHashMap<K, V> {

    private final Map<K, List<V>> map = new HashMap<>();

    public void add(K key, V value) {
        ensureListFor(key).add(value);
    }

    private List<V> ensureListFor(K key) {
        List<V> list = map.get(key);
        if (list == null) {
            list = new ArrayList<>();
            map.put(key, list);
        }

        return list;
    }

    public List<V> get(K key) {
        return map.get(key);
    }

}

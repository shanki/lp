/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Mutable.
 *
 * @author shanki
 *
 */
public class AnswerSet extends HashSet<Literal> {

    public AnswerSet() {
    }

    public AnswerSet(int initialCapacity) {
        super(initialCapacity);
    }

    public AnswerSet(Collection<Literal> literals) {
        super(Objects.requireNonNull(literals, "literals cannot be null"));
    }

    @Override
    public boolean add(Literal literal) {
        Objects.requireNonNull(literal, "literal cannot be null");

        return super.add(literal);
    }

    AnswerSet rewriteIdsToObjects(ObjectConstantMapping cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        AnswerSet rewritten = new AnswerSet(size());

        for (Literal literal : this) {
            rewritten.add(literal.rewriteIdsToObjectConstants(cache));
        }

        return rewritten;
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printAnswerSet(this);
    }

    public AnswerSet restrictToLiterals(Collection<Literal> domain) {
        Objects.requireNonNull(domain, "domain cannot be null");

        AnswerSet ret = new AnswerSet(this);
        ret.retainAll(domain);

        return ret;
    }

}

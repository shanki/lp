/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Mutable.
 *
 * @author shanki
 */
public class WeakConstraints extends ArrayList<WeakConstraint> {

    public WeakConstraints() {
    }

    public WeakConstraints(int initialCapacity) {
        super(initialCapacity);
    }

    public WeakConstraints(Collection<WeakConstraint> weaks) {
        super(Objects.requireNonNull(weaks, "weaks cannot be null"));
    }

    Collection<Atom> collectAllAtoms() {
        Set<Atom> atoms = new HashSet<>();

        for (WeakConstraint weak : this) {
            atoms.addAll(weak.collectAllAtoms());
        }

        return atoms;
    }

    Collection<? extends Literal> collectAllLiterals() {
        Set<Literal> literals = new HashSet<>();

        for (WeakConstraint w : this) {
            literals.addAll(w.collectAllLiterals());
        }

        return literals;
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printWeakConstraints(this);
    }

    public WeakConstraints rewriteElementsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        WeakConstraints ret = new WeakConstraints(size());

        for (WeakConstraint weak : this) {
            ret.add(weak.rewriteElementsToIds(mapping));
        }

        return ret;
    }

    public ViolationDegree computeViolationDegreeOf(AnswerSet as) {
        Objects.requireNonNull(as, "as cannot be null");

        ViolationDegreeComputer computer = new ViolationDegreeComputer();

        for (WeakConstraint weak : this) {
            computer.consume(weak, weak.isViolatedIn(as));
        }

        return computer.getValue();
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Immutable
 *
 * @author shanki
 */
public class ViolationDegree implements Comparable<ViolationDegree> {

    public static final ViolationDegree fromWithOwnership(List<BigDecimal> weights) {
        Objects.requireNonNull(weights, "weights cannot be null");

        return new ViolationDegree(weights);
    }

    private final List<BigDecimal> weights;

    private ViolationDegree(List<BigDecimal> weights) {
        this.weights = weights;
    }

    public int compareTo(ViolationDegree other, int ifNull) {
        return other == null ? ifNull : this.compareTo(other);
    }

    @Override
    public int compareTo(ViolationDegree other) {
        Objects.requireNonNull(other, "other cannot be null");

        if (this.weights.size() != other.weights.size()) {
            throw new IllegalArgumentException("other must be of the same length as this.");
        }

        Iterator<BigDecimal> i1 = this.weights.iterator();
        Iterator<BigDecimal> i2 = other.weights.iterator();

        while (i1.hasNext()) {
            BigDecimal v1 = i1.next();
            BigDecimal v2 = i2.next();

            int c = v1.compareTo(v2);

            if (c != 0) {
                return c;
            }
        }

        return 0;
    }

    @Override
    public String toString() {
        return weights.toString();
    }

}

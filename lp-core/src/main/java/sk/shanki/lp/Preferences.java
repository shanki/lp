/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Mutable.
 *
 * @author shanki
 */
public class Preferences extends ArrayList<Preference> {

    public Preferences(Collection<Preference> preferences) {
        super(Objects.requireNonNull(preferences, "preferences cannot be null"));
    }

    public Preferences() {
    }

    Program toMetaProgram() {
        Program meta = new Program();
        for (Preference preference : this) {
            meta.add(preference.toMetaRule());
        }

        return meta;
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printPreferences(this);
    }

}

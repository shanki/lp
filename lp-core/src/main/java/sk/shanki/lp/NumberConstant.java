/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * @author shanki
 */
public class NumberConstant extends BaseConstant {

    private final BigDecimal number;
    private transient int hashCode;

    public NumberConstant(BigDecimal number) {
        this.number = Objects.requireNonNull(number, "number cannot be null");
    }

    public NumberConstant(int number) {
        this.number = new BigDecimal(number);
    }

    public BigDecimal getValue() {
        return number;
    }

    public int intValue() {
        return number.intValue();
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 3;
            hash = 83 * hash + number.hashCode();
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NumberConstant other = (NumberConstant) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        return Objects.equals(this.number, other.number);
    }

    public NumberConstant abs() {
        BigDecimal abs = number.abs();

        return abs == number ? this : new NumberConstant(abs);
    }

    public NumberConstant add(NumberConstant other) {
        Objects.requireNonNull(other, "other cannot be null");

        return new NumberConstant(this.number.add(other.number));
    }

    public NumberConstant subtract(NumberConstant other) {
        Objects.requireNonNull(other, "other cannot be null");

        return new NumberConstant(this.number.subtract(other.number));
    }

    public NumberConstant pow(NumberConstant other) {
        Objects.requireNonNull(other, "other cannot be null");

        return new NumberConstant(this.number.pow(other.number.intValueExact()));
    }

    public NumberConstant and(NumberConstant other) {
        Objects.requireNonNull(other, "other cannot be null");

        return new NumberConstant(new BigDecimal(this.number.toBigIntegerExact().and(other.number.toBigIntegerExact())));
    }

    @Override
    public int compareTo(Term other, ObjectConstantMapping cache) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(cache, "cache cannot be null");

        if (other instanceof NumberConstant) {
            NumberConstant o = (NumberConstant) other;

            return this.number.compareTo(o.number);
        } else {
            return compareToDifferent(other);
        }
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printNumber(number);
    }

    @Override
    public int getPriority() {
        return 1;
    }

}

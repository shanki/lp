/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import sk.shanki.lp.util.MultiHashMap;

/**
 *
 * @author shanki
 */
public class IndexedLiterals {

    public static IndexedLiterals from(Iterable<Literal> literals) {
        return new IndexedLiterals(literals);
    }

    private final MultiHashMap<String, Literal> bySymbol = new MultiHashMap<>();
    private final MultiHashMap<String, Literal> byType = new MultiHashMap<>();

    private IndexedLiterals(Iterable<Literal> literals) {
        Objects.requireNonNull(literals, "literals cannot be null");

        for (Literal literal : literals) {
            bySymbol.add(literal.getSymbol(), literal);
            byType.add(literal.getType(), literal);
        }
    }

    public Literal oneOfSymbol(String symbol) {
        Objects.requireNonNull(symbol, "symbol cannot be null");

        List<Literal> list = bySymbol.get(symbol);
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    public List<Literal> allOfSymbol(String symbol) {
        Objects.requireNonNull(symbol, "symbol cannot be null");

        List<Literal> list = bySymbol.get(symbol);
        return list != null ? list : Collections.emptyList();
    }

    public Literal oneOfType(String type) {
        Objects.requireNonNull(type, "type cannot be null");

        List<Literal> list = byType.get(type);
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    public List<Literal> allOfType(String type) {
        Objects.requireNonNull(type, "type cannot be null");

        List<Literal> list = byType.get(type);
        return list != null ? list : Collections.emptyList();
    }

}

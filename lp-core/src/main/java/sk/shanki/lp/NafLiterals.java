/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 *
 */
public class NafLiterals implements Iterable<NafLiteral> {

    private static final NafLiterals EMPTY = new NafLiterals(Collections.emptyList());

    public static final NafLiterals empty() {
        return EMPTY;
    }

    public static final NafLiterals fromWithOwnership(NafLiteral... nafLiterals) {
        return new NafLiterals(Collections.unmodifiableList(Arrays.asList(nafLiterals)));
    }

    public static final NafLiterals fromWithOwnership(List<? extends NafLiteral> nafLiterals) {
        return new NafLiterals(Collections.unmodifiableList(nafLiterals));
    }

    public static final NafLiterals from(NafLiteral nafLiteral) {
        return new NafLiterals(Collections.singletonList(nafLiteral));
    }

    public static final NafLiterals from(Collection<? extends NafLiteral> nafLiterals) {
        return new NafLiterals(Collections.unmodifiableList(new ArrayList<>(nafLiterals)));
    }

    private final List<NafLiteral> list;

    private NafLiterals(List<NafLiteral> nafLiterals) {
        list = nafLiterals;
    }

    public NafLiterals add(NafLiteral nafLiteral) {
        Objects.requireNonNull(nafLiteral, "nafLiteral cannot be null");

        List<NafLiteral> newList = new ArrayList<>(list.size() + 1);
        newList.addAll(list);
        newList.add(nafLiteral);

        return NafLiterals.fromWithOwnership(newList);
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NafLiterals == false) {
            return false;
        }

        NafLiterals other = (NafLiterals) obj;

        return list.equals(other.list);
    }

    @Override
    public Iterator<NafLiteral> iterator() {
        return list.iterator();
    }

    public boolean isGround() {
        for (NafLiteral nafLiteral : this) {
            if (nafLiteral.isGround() == false) {
                return false;
            }
        }

        return true;
    }

    public NafLiterals fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        // TODO: urobit na jeden prechod
        if (willChange(substitution) == false) {
            return this;
        }

        List<NafLiteral> substituted = new ArrayList<>(size());

        for (NafLiteral nafLiteral : this) {
            substituted.add(nafLiteral.fullySubstitute(substitution));
        }

        return NafLiterals.fromWithOwnership(substituted);
    }

    public NafLiterals partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        // TODO: urobit to na jeden prechod
        if (willChange(substitution) == false) {
            return this;
        }

        List<NafLiteral> substituted = new ArrayList<>(size());

        for (NafLiteral nafLiteral : this) {
            substituted.add(nafLiteral.partiallySubstitute(substitution));
        }

        return NafLiterals.fromWithOwnership(substituted);
    }

    public boolean isSingleton() {
        return size() == 1;
    }

    public NafLiteral getSingleton() {
        return list.get(0);
    }

    Set<Atom> collectAllAtoms() {
        Set<Atom> atoms = new HashSet<>(size());

        for (NafLiteral nafLiteral : this) {
            atoms.add(nafLiteral.getAtom());
        }

        return atoms;
    }

    Set<Literal> collectAllLiterals() {
        Set<Literal> literals = new HashSet<>(size());

        for (NafLiteral nafLiteral : this) {
            literals.add(nafLiteral.getLiteral());
        }

        return literals;
    }

    boolean containsNot() {
        for (NafLiteral nafLiteral : this) {
            if (nafLiteral instanceof Not) {
                return true;
            }
        }

        return false;
    }

    public Literals getPositiveLiterals() {
        ArrayList<Literal> literals = new ArrayList<>();

        for (NafLiteral nafLiteral : this) {
            if (nafLiteral instanceof Literal) {
                literals.add((Literal) nafLiteral);
            }
        }

        return Literals.fromWithOwnership(literals);
    }

    public Literals getNegativeLiterals() {
        ArrayList<Literal> literals = new ArrayList<>();

        for (NafLiteral nafLiteral : this) {
            if (nafLiteral instanceof Not) {
                literals.add(((Not) nafLiteral).getLiteral());
            }
        }

        return Literals.fromWithOwnership(literals);
    }

    boolean isSatisfiedIn(AnswerSet answerSet) {
        Objects.requireNonNull(answerSet, "answerSet cannot be null");

        for (NafLiteral nafLiteral : this) {
            if (nafLiteral.isSatisfiedIn(answerSet) == false) {
                return false;
            }
        }

        return true;
    }

    public ProgramPrinter print(ProgramPrinter printer, String separator) {
        Objects.requireNonNull(printer, "printer cannot be null");
        Objects.requireNonNull(separator, "separator cannot be null");

        return printer.printNafLiterals(this, separator);
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter(), " ").toString();
    }

    public NafLiterals rewriteElementsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        // TODO: aby ak sa nic nezmeni sme nevytvarali instanciu listu
        List<NafLiteral> ret = new ArrayList<>(size());
        boolean changed = false;

        for (NafLiteral nafLiteral : this) {
            NafLiteral rewrittenLiteral = nafLiteral.rewriteObjectConstantsToIds(mapping);
            ret.add(rewrittenLiteral);

            if (nafLiteral != rewrittenLiteral) {
                changed = true;
            }
        }

        return changed ? NafLiterals.fromWithOwnership(ret) : this;
    }

    public NafLiterals getPositive() {
        ArrayList<NafLiteral> literals = new ArrayList<>();

        for (NafLiteral nafLiteral : this) {
            if (nafLiteral instanceof Literal) {
                literals.add(nafLiteral);
            }
        }

        return NafLiterals.fromWithOwnership(literals);
    }

    boolean willChange(Substitution substitution) {
        for (NafLiteral nafLiteral : this) {
            if (nafLiteral.willChange(substitution)) {
                return true;
            }
        }

        return false;
    }

}

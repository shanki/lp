/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Collection;
import java.util.Objects;
import sk.shanki.lp.predicates.GroundingPredicateFactory;
import sk.shanki.lp.grounder.GroundingConstraint;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Immutable.
 *
 * @author shanki
 */
public class Constraint {

    static Constraint forNegation(Atom tuple) {
        NafLiterals body = NafLiterals.fromWithOwnership(tuple, tuple.negate());

        return new Constraint(body);
    }

    private final NafLiterals body;
    private transient int hashCode;

    public Constraint(NafLiterals body) {
        this.body = Objects.requireNonNull(body, "body cannot be null");
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 7;
            hash = 53 * hash + body.hashCode();
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Constraint other = (Constraint) obj;
        return this.body.equals(other.body);
    }

    Rule toRule() {
        NafLiterals newHead = NafLiterals.from(Inconsistency.getInstance());
        NafLiterals newBody = body.add(new Not(Inconsistency.getInstance()));

        return new Rule(newHead, newBody);
    }

    GroundingConstraint toGroundingConstraint(GroundingPredicateFactory factory) {
        Objects.requireNonNull(factory, "factory cannot be null");

        Literals posBody = body.getPositiveLiterals();
        Literals negBody = body.getNegativeLiterals();

        Literals.Carret split = posBody.split(factory);

        return new GroundingConstraint(split.literals, negBody, split.predicates);
    }

    Collection<? extends Literal> collectAllLiterals() {
        return body.collectAllLiterals();
    }

    Collection<? extends Atom> collectAllAtoms() {
        return body.collectAllAtoms();
    }

    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printConstraint(body);
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

    Constraint rewriteElementsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        NafLiterals rewrittenBody = body.rewriteElementsToIds(mapping);

        // object allocation optimization
        return body == rewrittenBody ? this : new Constraint(rewrittenBody);
    }
}

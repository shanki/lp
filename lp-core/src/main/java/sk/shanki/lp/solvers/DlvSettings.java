/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp.solvers;

/**
 *
 * @author shanki
 */
public class DlvSettings {

    public static DlvSettings newInstance() {
        return new DlvSettings();
    }

    private String path;

    private DlvSettings() {
        // user factory method
    }

    public DlvSettings setPath(String path) {
        this.path = path;
        return this;
    }

    public String getPath() {
        return path;
    }

    public String getPathOrDefault(String defaultValue) {
        return path != null ? path : defaultValue;
    }

}

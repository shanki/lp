/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp.solvers;

/**
 *
 * @author shanki
 */
public class ClingoSettings {

    public static ClingoSettings newInstance() {
        return new ClingoSettings();
    }

    private String path;

    private ClingoSettings() {
        // user factory method
    }

    public ClingoSettings setPath(String path) {
        this.path = path;
        return this;
    }

    public String getPath() {
        return path;
    }

    public String getPathOrDefault(String defaultValue) {
        return path != null ? path : defaultValue;
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import sk.shanki.lp.predicates.GroundingPredicateFactory;
import sk.shanki.lp.grounder.GroundingWeakConstraint;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class WeakConstraint {

    private final NafLiterals body;
    private final BigDecimal weight;
    private final BigDecimal level;
    private final Terms terms;

    private transient int hashCode;

    public WeakConstraint(NafLiterals body, BigDecimal weight, BigDecimal level, Terms terms) {
        this.body = Objects.requireNonNull(body, "body cannot be null");
        this.weight = Objects.requireNonNull(weight, "weight cannot be null");
        this.level = Objects.requireNonNull(level, "level cannot be null");
        this.terms = Objects.requireNonNull(terms, "terms cannot be null");
    }

    GroundingWeakConstraint toGroundingWeakConstraint(GroundingPredicateFactory factory) {
        Objects.requireNonNull(factory, "factory cannot be null");

        Literals posBody = body.getPositiveLiterals();
        Literals negBody = body.getNegativeLiterals();

        Literals.Carret split = posBody.split(factory);

        return new GroundingWeakConstraint(split.literals, negBody, split.predicates, weight, level, terms);

    }

    Collection<? extends Literal> collectAllLiterals() {
        return body.collectAllLiterals();
    }

    Collection<? extends Atom> collectAllAtoms() {
        return body.collectAllAtoms();
    }

    public void print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        printer.printWeakConstraint(body, weight, level, terms);
    }

    WeakConstraint rewriteElementsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        NafLiterals rewrittenBody = body.rewriteElementsToIds(mapping);
        Terms rewrittenTerms = terms.rewriteObjectConstantsToIds(mapping);

        return body == rewrittenBody && terms == rewrittenTerms
                ? this
                : new WeakConstraint(rewrittenBody, weight, level, rewrittenTerms);
    }

    BigDecimal getLevel() {
        return level;
    }

    WeakConstraintTuple toTuple() {
        return new WeakConstraintTuple(weight, terms);
    }

    boolean isViolatedIn(AnswerSet as) {
        Objects.requireNonNull(as, "as cannot be null");

        return body.isSatisfiedIn(as);
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            int hash = 3;
            hash = 43 * hash + Objects.hashCode(this.body);
            hash = 43 * hash + Objects.hashCode(this.weight);
            hash = 43 * hash + Objects.hashCode(this.level);
            hash = 43 * hash + Objects.hashCode(this.terms);
            hashCode = hash;
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WeakConstraint other = (WeakConstraint) obj;

        if (!this.body.equals(other.body)) {
            return false;
        }

        if (!this.weight.equals(other.weight)) {
            return false;
        }

        if (!this.level.equals(other.level)) {
            return false;
        }

        if (!this.terms.equals(other.terms)) {
            return false;
        }

        return true;
    }

}

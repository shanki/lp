/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import sk.shanki.lp.predicates.GroundingPredicateFactory;
import sk.shanki.lp.compilation.CompilationFactory;
import sk.shanki.lp.compilation.CompiledLiteral;
import sk.shanki.lp.compilation.CompiledLiteralCache;
import sk.shanki.lp.compilation.CompiledLiterals;
import sk.shanki.lp.compilation.CompiledProgram;
import sk.shanki.lp.compilation.CompiledRule;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.smodels.SmodelsLiteral;
import sk.shanki.lp.smodels.SmodelsLiteralCache;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class Literals implements Iterable<Literal> {

    public static final Literals fromWithOwnership(List<Literal> list) {
        return new Literals(list);
    }

    private final List<Literal> list;

    private Literals(List<Literal> literals) {
        this.list = Collections.unmodifiableList(literals);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int size() {
        return list.size();
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Literals == false) {
            return false;
        }

        Literals other = (Literals) obj;

        return list.equals(other.list);
    }

    @Override
    public Iterator<Literal> iterator() {
        return list.iterator();
    }

    public boolean isGround() {
        for (Literal literal : list) {
            if (literal.isGround() == false) {
                return false;
            }
        }

        return false;
    }

    public Literals fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        // TODO: return this when literals do not change
        List<Literal> substituted = new ArrayList<>(size());

        for (Literal literal : list) {
            substituted.add(literal.fullySubstitute(substitution));
        }

        return new Literals(substituted);
    }

    public List<SmodelsLiteral> toSmodelsLiteral(SmodelsLiteralCache cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        List<SmodelsLiteral> compiled = new ArrayList<>(size());

        for (Literal literal : this) {
            compiled.add(cache.ensure(literal));
        }

        return compiled;
    }

    public <P extends CompiledProgram, R extends CompiledRule, L extends CompiledLiteral, LS extends CompiledLiterals> LS compile(CompilationFactory<P, R, L, LS> factory, CompiledLiteralCache<L> cache) {
        Objects.requireNonNull(factory, "factory cannot be null");
        Objects.requireNonNull(cache, "cache cannot be null");

        List<L> compiled = new ArrayList<>(size());

        for (Literal literal : this) {
            compiled.add(cache.ensure(literal));
        }

        return factory.createLiterals(compiled);
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter(), ", ").toString();
    }

    public ProgramPrinter print(ProgramPrinter printer, String separator) {
        Objects.requireNonNull(printer, "printer cannot be null");
        Objects.requireNonNull(separator, "separator cannot be null");

        return printer.printLiterals(this, separator);
    }

    Carret split(GroundingPredicateFactory factory) {
        Objects.requireNonNull(factory, "factory cannot be null");

        List<Literal> lits = new ArrayList<>();
        List<GroundingPredicate> predicates = new ArrayList<>();

        for (Literal literal : this) {
            GroundingPredicate p = factory.create(literal);

            if (p == null) {
                lits.add(literal);
            } else {
                predicates.add(p);
            }
        }

        return new Carret(Literals.fromWithOwnership(lits), new GroundingPredicates(predicates));
    }

    public static class Carret {

        public Literals literals;
        public GroundingPredicates predicates;

        public Carret(Literals literals, GroundingPredicates predicates) {
            this.literals = literals;
            this.predicates = predicates;
        }
    }
}

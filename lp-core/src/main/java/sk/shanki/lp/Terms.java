/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;
import sk.shanki.lp.util.Validation;

/**
 *
 * Immutable.
 *
 * @author shanki
 */
public class Terms implements Iterable<Term> {

    private static final Terms EMPTY = new Terms(Collections.EMPTY_LIST);

    public static final Terms empty() {
        return EMPTY;
    }

    public static final Terms fromWithOwnership(List<? extends Term> terms) {
        Objects.requireNonNull(terms, "terms cannot be null");
        Validation.requireAllNonNull(terms, "each term must be non-null");

        if (terms.isEmpty()) {
            return empty();
        }

        return new Terms(Collections.unmodifiableList(terms));
    }

    public static final Terms fromWithOwnership(Term... terms) {
        Objects.requireNonNull(terms, "terms cannot be null");
        Validation.requireAllNonNull(terms, "each term must be non-null");

        if (terms.length == 0) {
            return empty();
        }

        return new Terms(Collections.unmodifiableList(Arrays.asList(terms)));
    }

    private final List<Term> list;
    private transient int hashCode = 0;

    private Terms(List<Term> terms) {
        list = terms;
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int size() {
        return list.size();
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = list.hashCode();
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Terms == false) {
            return false;
        }

        Terms other = (Terms) obj;

        return list.equals(other.list);
    }

    @Override
    public Iterator<Term> iterator() {
        return list.iterator();
    }

    public Term get(int index) {
        return list.get(index);
    }

    boolean unify(Terms other, Substitution substitution) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(substitution, "substitution cannot be null");

        int size = this.size();

        if (size != other.size()) {
            return false;
        }

        for (int i = 0; i < size; ++i) {
            Term thisTerm = this.get(i);
            Term otherTerm = other.get(i);

            if (thisTerm.unify(otherTerm, substitution) == false) {
                return false;
            }
        }

        return true;
    }

    boolean isGround() {
        for (Term term : this) {
            if (term.isGround() == false) {
                return false;
            }
        }

        return true;
    }

    public Terms fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        if (willChange(substitution) == false) {
            return this;
        }

        ArrayList<Term> substituted = new ArrayList<>(size());

        for (Term term : this) {
            substituted.add(term.fullySubstitute(substitution));
        }

        return Terms.fromWithOwnership(substituted);
    }

    boolean willChange(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        for (Term term : this) {
            if (term.willChange(substitution)) {
                return true;
            }
        }

        return false;
    }

    Terms partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        if (willChange(substitution) == false) {
            return this;
        }

        ArrayList<Term> substituted = new ArrayList<>(size());

        for (Term term : this) {
            substituted.add(term.partiallySubstitute(substitution));
        }

        return Terms.fromWithOwnership(substituted);
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter(), " ").toString();
    }

    Terms rewriteObjectConstantsToIds(ObjectConstantMapping cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        // TODO: zbavit sa alovacie ak sa nic nemeni
        ArrayList<Term> rewritten = new ArrayList<>(size());
        boolean changed = false;

        for (Term term : this) {
            Term rewrittenTerm = term.rewriteObjectConstantsToIds(cache);
            rewritten.add(rewrittenTerm);

            if (term != rewrittenTerm) {
                changed = true;
            }
        }

        return changed ? Terms.fromWithOwnership(rewritten) : this;
    }

    Terms rewriteIdsToObjectConstants(ObjectConstantMapping cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        // TODO: zbavit sa alovacie ak sa nic nemeni
        ArrayList<Term> rewritten = new ArrayList<>(size());
        boolean changed = false;

        for (Term term : this) {
            Term rewrittenTerm = term.rewriteIdsToObjectConstants(cache);
            rewritten.add(rewrittenTerm);

            if (term != rewrittenTerm) {
                changed = true;
            }
        }

        return changed ? Terms.fromWithOwnership(rewritten) : this;
    }

    public ProgramPrinter print(ProgramPrinter printer, String separator) {
        Objects.requireNonNull(printer, "printer cannot be null");
        Objects.requireNonNull(separator, "separator cannot be null");

        boolean isFirst = true;

        for (Term term : this) {
            if (isFirst == false) {
                printer.printSeparator(separator);
            } else {
                isFirst = false;
            }

            term.print(printer);
        }

        return printer;
    }

    int compareTo(Terms other, ObjectConstantMapping mapping) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(mapping, "mapping cannot be null");

        int thisSize = this.size();
        int otherSize = other.size();

        int a = Integer.compare(thisSize, otherSize);
        if (a != 0) {
            return a;
        }

        for (int i = 0; i < thisSize; ++i) {
            Term thisElement = this.get(i);
            Term otherElement = other.get(i);

            int b = thisElement.compareTo(otherElement, mapping);
            if (b != 0) {
                return b;
            }
        }

        return 0;
    }

}

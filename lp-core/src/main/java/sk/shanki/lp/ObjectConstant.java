/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Itself immutable, but there is no guarantee whether the underlying object is
 * immutable or not.
 *
 * @author shanki
 */
public class ObjectConstant extends BaseConstant {

    private final Object value;
    private transient int hashCode;

    public ObjectConstant(Object value) {
        // TODO: moze byt value null ?
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public int compareTo(Term other, ObjectConstantMapping cache) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(cache, "cache cannot be null");

        if (other instanceof ObjectConstant) {
            ObjectConstant o = (ObjectConstant) other;

            return cache.toId(this).compareTo(cache.toId(o), null);
        } else {
            return compareToDifferent(other);
        }
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = 553 + Objects.hashCode(this.value);
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ObjectConstant other = (ObjectConstant) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        return Objects.equals(this.value, other.value);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printObject(value);
    }

    @Override
    public int getPriority() {
        return 3;
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;

/**
 *
 * Immutable.
 *
 * @author shanki
 */
public abstract class BaseConstant extends BaseTerm {

    @Override
    public boolean unify(Term other, Substitution substitution) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(substitution, "substitution cannot be null");

        return this.equals(other);
    }

    @Override
    public boolean isGround() {
        return true;
    }

    @Override
    public Term fullySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        return this;
    }

    @Override
    public Term partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        return this;
    }

    @Override
    public boolean willChange(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        return false;
    }

    @Override
    public Term rewriteIdsToObjectConstants(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        return this;
    }

    @Override
    public Term rewriteObjectConstantsToIds(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        return this;
    }

}

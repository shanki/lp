/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import sk.shanki.lp.exceptions.SolverException;
import sk.shanki.lp.printing.StandardProgramPrinter;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 *
 * Mutable.
 *
 * @author shanki
 *
 */
public class AnswerSets extends HashSet<AnswerSet> {

    public AnswerSets() {
    }

    public AnswerSets(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public boolean add(AnswerSet answerSet) {
        Objects.requireNonNull(answerSet, "answerSet cannot be null");

        return super.add(answerSet);
    }

    public AnswerSets rewriteIdsToObjects(ObjectConstantMapping cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        AnswerSets sets = new AnswerSets(size());

        for (AnswerSet as : this) {
            sets.add(as.rewriteIdsToObjects(cache));
        }

        return sets;
    }

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printAnswerSets(this);
    }

    public AnswerSet first() {
        return iterator().next();
    }

    public AnswerSets filter(AnswerSetChecker checker) throws SolverException {
        Objects.requireNonNull(checker, "checker cannot be null");

        AnswerSets ret = new AnswerSets();

        for (AnswerSet as : this) {
            if (checker.isAnswerSetOk(as)) {
                ret.add(as);
            }
        }

        return ret;
    }

    public AnswerSets restrictToLiterals(Collection<Literal> literals) {
        Objects.requireNonNull(literals, "literals cannot be null");

        AnswerSets ret = new AnswerSets(size());

        for (AnswerSet as : this) {
            ret.add(as.restrictToLiterals(literals));
        }

        return ret;

    }

    public AnswerSets optimal(WeakConstraints weakConstraints) {
        Objects.requireNonNull("weakConstraints cannot be null");

        Collector collector = new Collector(weakConstraints, 0);

        for (AnswerSet as : this) {
            collector.add(as);
        }

        return collector.getAnswerSets();
    }

}

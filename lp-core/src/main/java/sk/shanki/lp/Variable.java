/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import sk.shanki.lp.exceptions.UnboundVariableException;
import java.util.Objects;
import sk.shanki.lp.printing.ProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public class Variable extends BaseTerm {

    private final String name;
    private transient int hashCode;

    public Variable(String name) {
        this.name = Objects.requireNonNull(name, "name cannot be null");
    }

    protected Variable makeCopy() {
        return new Variable(name);
    }

    @Override
    public boolean unify(Term other, Substitution substitution) {
        Objects.requireNonNull(other, "other cannot be null");
        Objects.requireNonNull(substitution, "substitution cannot be null");

        return substitution.put(this, other);
    }

    @Override
    public Variable rewriteObjectConstantsToIds(ObjectConstantMapping cache) {
        Objects.requireNonNull(cache, "cache cannot be null");

        return this;
    }

    @Override
    public Variable rewriteIdsToObjectConstants(ObjectConstantMapping mapping) {
        Objects.requireNonNull(mapping, "mapping cannot be null");

        return this;
    }

    @Override
    public boolean isGround() {
        return false;
    }

    @Override
    public Term fullySubstitute(Substitution substitution) throws UnboundVariableException {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        Term term = substitution.get(this);

        if (term == null) {
            throw new UnboundVariableException(this);
        } else {
            return term;
        }
    }

    @Override
    public Term partiallySubstitute(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        Term term = substitution.get(this);

        if (term == null) {
            // vzdy vratime kopiu, kvoli kontextom, v ktorych sa premenne pouzivaju (potrebne pri unifikovani)
            return makeCopy();
        } else {
            return term;
        }
    }

    @Override
    public boolean willChange(Substitution substitution) {
        Objects.requireNonNull(substitution, "substitution cannot be null");

        // vzdy vratime kopiu, kvoli kontextom, v ktorych sa premenne pouzivaju (potrebne pri unifikovani)
        return true;
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = 511 + name.hashCode();
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Variable other = (Variable) obj;

        if (this.hashCode() != other.hashCode()) {
            return false;
        }

        return this.name.equals(other.name);
    }

    @Override
    public ProgramPrinter print(ProgramPrinter printer) {
        Objects.requireNonNull(printer, "printer cannot be null");

        return printer.printVariable(name);
    }

    @Override
    public int getPriority() {
        return 5;
    }

    @Override
    public int compareTo(Term element, ObjectConstantMapping mapping) {
        Objects.requireNonNull(element, "element cannot be null");
        Objects.requireNonNull(mapping, "mapping cannot be null");

        if (element instanceof Variable) {
            Variable other = (Variable) element;

            return this.name.compareTo(other.name);
        } else {
            return compareToDifferent(element);
        }
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp;

import java.util.Objects;
import sk.shanki.lp.printing.StandardProgramPrinter;

/**
 * Immutable.
 *
 * @author shanki
 */
public abstract class BaseTerm implements Term {

    @Override
    public String toString() {
        return print(new StandardProgramPrinter()).toString();
    }

    public int compareToDifferent(Term element) {
        Objects.requireNonNull(element, "element cannot be null");

        return Integer.compare(getPriority(), element.getPriority());
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.lp.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;
import sk.shanki.lp.solvers.ClingoSettings;
import sk.shanki.lp.solvers.ClingoSolver;

/**
 *
 * @author shanki
 */
public class ClingoTest {

    private static ClingoSettings settings;

    @BeforeClass
    public static void setUp() {
        settings = ClingoSettings.newInstance().setPath("/opt/local/bin/");
    }

    @Test
    public void testEmptyProgram() throws Exception {
        String p = "a :- b. b :- a.";
        ClingoSolver s = new ClingoSolver(settings);
        String out = s.run(p, 0);

        assertNotNull(out);
        assertFalse(out.isEmpty());
    }

}

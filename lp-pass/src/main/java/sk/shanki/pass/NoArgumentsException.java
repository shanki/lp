/*
 * Copyright shanki. All rights reserved.
 */

package sk.shanki.pass;

/**
 *
 * @author shanki
 */
public class NoArgumentsException extends Exception {

    public NoArgumentsException() {
        super("No arguments.");
    }

}

/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.pass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import sk.shanki.lp.AnswerSet;
import sk.shanki.lp.AnswerSets;
import sk.shanki.lp.Program;
import sk.shanki.lp.exceptions.SemanticException;
import sk.shanki.lp.exceptions.SolverException;
import sk.shanki.lp.exceptions.UnknownSolverException;
import sk.shanki.lp.parser.ProgramFactory;
import sk.shanki.lp.solvers.ClingoSettings;
import sk.shanki.lp.solvers.DlvSettings;
import sk.shanki.lp.solvers.Solver;
import sk.shanki.lp.solvers.SolverFactory;

/**
 *
 * @author shanki
 */
public class Main {

    private final ClingoSettings clingoSettings = ClingoSettings.newInstance();
    private final DlvSettings dlvSettings = DlvSettings.newInstance();
    private final SolverFactory solverFactory = new SolverFactory(clingoSettings, dlvSettings);

    private List<String> filenames = new ArrayList<>();
    private List<String> solverNames = new ArrayList<>();
    private boolean debug = false;

    private Mode mode = Mode.filename;

    private enum Mode {
        solver,
        filename,
        clingoPath,
        dlvPath
    }

    public void parseArgs(String[] args) throws NoArgumentsException {
        if (args.length == 0) {
            throw new NoArgumentsException();
        }

        int index = 0;

        while (index < args.length) {
            String arg = args[index];
            switch (arg) {
                case "-s":
                    mode = Mode.solver;
                    ++index;
                    break;
                case "-d":
                    debug = true;
                    ++index;
                    break;
                case "-cp":
                    mode = Mode.clingoPath;
                    ++index;
                    break;
                case "-dp":
                    mode = Mode.dlvPath;
                    ++index;
                    break;
                default:
                    setData(arg);
                    ++index;
                    break;

            }
        }
    }

    private void setData(String arg) throws IllegalStateException {
        switch (mode) {
            case filename:
                filenames.add(arg);
                break;
            case solver:
                solverNames.add(arg);
                break;
            case clingoPath:
                clingoSettings.setPath(arg);
                break;
            case dlvPath:
                dlvSettings.setPath(arg);
                break;
            default:
                throw new IllegalStateException("unknown parsing mode");
        }
    }

    public void run() throws IOException, SolverException, UnknownSolverException {
        Solver solver = solverFactory.create(solverNames);

        ProgramFactory factory = new ProgramFactory();
        Program total = new Program();

        for (String filename : filenames) {
            Program actual = factory.fromFile(filename);

            total.add(actual);
        }

        AnswerSets ass = solver.evaluate(total, 0);

        for (AnswerSet set : ass) {
            System.out.println(set);
        }
    }

    public void debug() throws IOException, SemanticException, UnknownSolverException {
        Solver solver = solverFactory.create(solverNames);

        ProgramFactory factory = new ProgramFactory();
        Program total = new Program();

        for (String filename : filenames) {
            Program actual = factory.fromFile(filename);

            total.add(actual);
        }

        String out = solver.debug(total);

        System.out.println(out);
    }

    public static void main(String[] args) {
        try {
            Main main = new Main();
            main.parseArgs(args);

            if (main.debug) {
                main.debug();
            } else {
                main.run();
            }

            System.exit(0);
        } catch (NoArgumentsException | SolverException | UnknownSolverException | IOException ex) {

            System.err.println("ERROR: " + ex.toString());
            System.exit(1);
        }

    }
}

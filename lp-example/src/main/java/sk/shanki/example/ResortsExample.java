/*
 * Copyright shanki. All rights reserved.
 */
package sk.shanki.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import sk.shanki.lp.AnswerSet;
import sk.shanki.lp.IndexedLiterals;
import sk.shanki.lp.Literal;
import sk.shanki.lp.solvers.ClingoSettings;
import sk.shanki.lp.solvers.ClingoSolver;

/**
 *
 * @author shanki
 */
public class ResortsExample {

    public static void main(String[] args) throws IOException {

        // Load program as string
        String program = Files.lines(Paths.get("resorts.txt")).collect(Collectors.joining(System.lineSeparator()));

        // Create a solver
        ClingoSettings settings = ClingoSettings.newInstance().setPath("/opt/local/bin/");
        ClingoSolver solver = new ClingoSolver(settings);

        // Evaluate the program
        AnswerSet as = solver.evaluateRaw(program, 0).first();

        // Create an index which allows to get literals by their symbol and type
        IndexedLiterals indexed = IndexedLiterals.from(as);

        Literal resortLiteral = indexed.oneOfType("resort/1");
        String resortId = resortLiteral.getSymbolicTerm(0);

        System.out.print("Resort ");
        System.out.println(resortId);

        Literal resortLengthLiteral = indexed.oneOfType("resort_length/2");
        int resortLength = resortLengthLiteral.getIntTerm(1);

        System.out.print("Resort length ");
        System.out.println(resortLength);

        for (Literal componentLengthLiteral : indexed.allOfType("component_length/2")) {
            String id = componentLengthLiteral.getSymbolicTerm(0);
            int length = componentLengthLiteral.getIntTerm(1);

            System.out.print("Component ");
            System.out.print(id);
            System.out.print(" has length ");
            System.out.println(length);
        }

    }

}
